## Building the container for FEniCS enviornment 

- sudo singularity build --sandbox FEniCS_env_sandbox $CURRENT_DIR/recipe/fenics_env.def

This line allow you to edit your outcome in terms of dependencies if anything is missing later. In order to do so, 

- sudo singularity shell --writable FEniCS_env_sandbox

Once the installation is done, 

- singularity exec FEniCS_env_sandbox python3 
- (in python3 prompt) import fenics 

The latter command will confirm whether FEniCS package is available or not. 

Once it is confirmed, execute the following line to create .sif file that is no longer mutatable. 

- sudo singularity build FEniCS_env FEniCS_env_sandbox


## Building the container for Gotran environment 

- sudo singularity build --sandbox Gotran_env_sandbox $CURRENT_DIR/recipe/gotran_env.def

This line allow you to edit your outcome in terms of dependencies if anything is missing later. In order to do so, 

- sudo singularity shell --writable Gotran_env_sandbox

Once the installation is done, 

- singularity exec Gotran_env_sandbox python3 
- (in python3 prompt) import gotran 

The latter command will confirm whether FEniCS package is available or not. 

Once it is confirmed, execute the following line to create .sif file that is no longer mutatable. 

- sudo singularity build Gotran_env Gotran_env_sandbox